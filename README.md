# OpenML dataset: Campus-Recruitment

https://www.openml.org/d/43307

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Hello
My name is Ben Roshan D, doing MBA in Business Analytics at Jain University Bangalore . We have practical sessions in Python,R as subjects. Faculties provide us with such data sets to work on with it, So here is one of the data set which our class worked on

What is in it
This data set consists of Placement data of students in a XYZ campus. It includes secondary and higher secondary school percentage and specialization. It also includes degree specialization, type and Work experience and salary offers to the placed students

Acknowledgement
I would like to thank Dr. Dhimant Ganatara, Professor Jain University for helping the students by providing this data for us to train R programming 

Questions
Which factor influenced a candidate in getting placed
Does  percentage matters for one to get placed
Which degree specialization is much demanded by corporate
Play with the data conducting all statistical tests.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43307) of an [OpenML dataset](https://www.openml.org/d/43307). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43307/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43307/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43307/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

